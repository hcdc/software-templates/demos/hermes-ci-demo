# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import hermes_ci_demo  # noqa: F401
