# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

"""pytest configuration script for hermes-ci-demo."""

import pytest  # noqa: F401
