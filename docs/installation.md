<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(installation)=
# Installation

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of hermes-ci-demo!
```

To install the _hermes-ci-demo_ package, we
recommend that you install it from PyPi via

```bash
pip install hermes-ci-demo
```

Or install it directly from [the source code repository on
Gitlab][source code repository]
via:

```bash
pip install git+https://codebase.helmholtz.cloud/hcdc/software-templates/demos/hermes-ci-demo.git
```

The latter should however only be done if you want to access the
development versions.

[source code repository]: https://codebase.helmholtz.cloud/hcdc/software-templates/demos/hermes-ci-demo

(install-develop)=
## Installation for development

Please head over to our
`contributing guide <contributing>`{.interpreted-text role="ref"} for
installation instruction for development.
